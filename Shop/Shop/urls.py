from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required
from Shop import settings

from shop_app.models import Product, Profile, Category
from shop_app.views import ProductDetailView, SearchProductListView, CategoryProductListView, ProductListView, \
    ProfileView, CategoryListView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Shop.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^sign_up/$', 'shop_app.views.sign_up', name='sign_up'),
    url(r'^sign_in/$', 'shop_app.views.sign_in', name='sign_in'),
    url(r'^sign_out/$', 'shop_app.views.sign_out', name='sign_out'),
    url(r'^set_lang/$', 'shop_app.views.set_language', name='set_lang'),

    url(
        r'^profile/(?P<pk>\d+)$',
        login_required(ProfileView.as_view())
    ),
    url(r'^profile/$', 'shop_app.views.profile', name='profile'),
    url(r'^profile/edit/$', 'shop_app.views.edit_profile', name='edit_profile'),
    url(r'^profile/avatar/(?P<prof>\d+)$', 'shop_app.views.avatar_download', name='avatar_download'),


    url(
        r'^$',
        login_required(ProductListView.as_view()),
        name='index'
    ),

    url(
        r'^search/$',
        login_required(SearchProductListView.as_view()),
        name='search'
    ),

    url(
        r'^product/(?P<pk>\d+)$',
        login_required(ProductDetailView.as_view()),
        name='product'
    ),

    url(
        r'^cart/$',
        'shop_app.views.cart',
        name='cart'
    ),

    url(
        r'^cart/add/$',
        'shop_app.views.add_to_cart',
        name='add_to_cart'
    ),

    url(
        r'^cart/del/$',
        'shop_app.views.del_from_cart',
        name='del_from_cart'
    ),

    url(
        r'^orders/$',
        'shop_app.views.my_orders',
        name='my_orders'
    ),

    url(
        r'^categories/$',
        login_required(CategoryListView.as_view()),
        name='categories'
    ),

    url(
        r'^category/(?P<cat>\d+)$',
        login_required(CategoryProductListView.as_view()),
        name='category_prods'
    ),
    
    url(r'^manage/', include('manager_app.urls', namespace='manage')),

)

urlpatterns += patterns('',
    url(
        r'^media/(?P<path>.*)',
        'django.views.static.serve',
        { 'document_root': settings.MEDIA_ROOT }
    )
)