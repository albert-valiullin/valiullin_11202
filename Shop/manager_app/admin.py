from django.contrib import admin
from manager_app.models import *


@admin.register(Consignment)
class ConsignmentAdmin(admin.ModelAdmin):
    list_display = ('product', 'init_quantity', 'quantity_left')
    search_fields = ('product__name',)
    list_filter = ('product__name', 'init_quantity', 'quantity_left')


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'address', 'contact', 'email')
    search_fields = ('name', 'owner', 'address__city', 'contact', 'email')
    list_filter = ('owner', 'address__city')


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ('consignment', 'cost', 'manufacturer', 'receive_date', 'receive_person')
    search_fields = ('consignment__product__name', 'manufacturer__name', 'receive_date', 'receive_person__username')
    list_filter = ('consignment__product__name', 'manufacturer__name', 'manufacturer__address__city')
    date_hierarchy = 'receive_date'
