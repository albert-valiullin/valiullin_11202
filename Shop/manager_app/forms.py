from django import forms
from shop_app.models import Category, Characteristic


class CategoryCreateForm(forms.ModelForm):
    parent = forms.ModelChoiceField(queryset=Category.objects.all(), required=False)

    class Meta:
        model = Category


class CharacteristicForm(forms.ModelForm):
    class Meta:
        model = Characteristic