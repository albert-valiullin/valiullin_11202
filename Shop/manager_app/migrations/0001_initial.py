# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('rel_app', '0001_initial'),
        ('shop_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('init_quantity', models.PositiveIntegerField()),
                ('quantity_left', models.PositiveIntegerField()),
                ('product', models.ForeignKey(to='shop_app.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('owner', models.CharField(max_length=256)),
                ('contact', models.CharField(max_length=1024)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('web_site', models.URLField(max_length=254, null=True, blank=True)),
                ('address', models.ForeignKey(to='rel_app.Address')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cost', models.PositiveIntegerField()),
                ('receive_date', models.DateField(auto_now_add=True)),
                ('consignment', models.ForeignKey(to='manager_app.Consignment')),
                ('manufacturer', models.ForeignKey(to='manager_app.Manufacturer')),
                ('receive_person', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
