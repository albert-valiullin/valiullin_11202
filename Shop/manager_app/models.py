from django.db import models
from django.contrib.auth.models import User
from rel_app.models import Address
from shop_app.models import Product


class Manufacturer(models.Model):
    name = models.CharField(max_length=256)
    owner = models.CharField(max_length=256)
    address = models.ForeignKey(Address)
    contact = models.CharField(max_length=1024)
    email = models.EmailField(max_length=254, null=True, blank=True)
    web_site = models.URLField(max_length=254, null=True, blank=True)

    def __unicode__(self):
        return self.name


class Consignment(models.Model):
    product = models.ForeignKey(Product)
    init_quantity = models.PositiveIntegerField()
    quantity_left = models.PositiveIntegerField()

    def __unicode__(self):
        return u'%s: %s' % (self.product.name, self.init_quantity)


class Store(models.Model):
    consignment = models.ForeignKey(Consignment)
    cost = models.PositiveIntegerField()
    manufacturer = models.ForeignKey(Manufacturer)
    receive_date = models.DateField(auto_now_add=True)
    receive_person = models.ForeignKey(User)

    def __unicode__(self):
        return u'%s: %s | %s' % (self.consignment.name, self.manufacturer, self.receive_date)