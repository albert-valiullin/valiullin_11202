from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required

from rel_app.models import Order
from shop_app.models import Category


urlpatterns = patterns('',
    url(r'^$',
        login_required(ListView.as_view(
            model=Order,
            paginate_by=5,
            template_name="manager_app/all_orders.html"
        )),
        name="index"),
    url(r'new_product/', 'manager_app.views.new_product', name='new_product'),
    url(r'save_char/', 'manager_app.views.save_char', name='save_char'),
    url(
        r'all_orders/',
        login_required(ListView.as_view(
            model=Order,
            paginate_by=5,
            template_name="manager_app/all_orders.html"
        )),
        name='all_orders'
    ),

    url(r'^new_category/$', 'manager_app.views.new_category', name='new_category'),
        url(
        r'^categories/$',
        login_required(ListView.as_view(
            model=Category,
            template_name="manager_app/categories.html",
            queryset=Category.objects.filter(parent__isnull=True)
        )),
        name='categories'
    ),

)