import json
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.safestring import SafeString
from django.forms.models import model_to_dict

from shop_app.models import Product, Characteristic, ProductCharacteristic
from manager_app.forms import *


@login_required
def new_product(request):
    if request.POST:
        p = Product()
        p.name = request.POST["name"]
        p.category = Category.objects.get(id=request.POST["category"])
        p.price = request.POST["price"]
        p.description = request.POST["description"]
        if "image" in request.POST:
            p.image = request.POST["image"]
        p.save()
        for ch in request.POST:
            if ch.startswith('ch_value_'):
                ch_id = int(ch.replace('ch_value_', ''))
                char = Characteristic.objects.get(id=ch_id)
                prod_char = ProductCharacteristic(product=p, characteristic=char, value=request.POST[ch])
                prod_char.save()
        return HttpResponseRedirect(reverse('manage:new_product'))
    else:
        form = CharacteristicForm()
        return render(
            request,
            "manager_app/new_product.html",
            {
                "categories": Category.objects.all(),
                "characteristics": SafeString(serializers.serialize("json", Characteristic.objects.all())),
                "form": form
            }
        )


@login_required()
def new_category(request):
    c = Category()
    if request.method == 'POST':
        form = CategoryCreateForm(request.POST, instance=c)
    else:
        form = CategoryCreateForm(instance=c)

    if form.is_valid():
        c.save()
        form.save()
        return HttpResponseRedirect(reverse('manage:categories'))
    return render(
        request,
        "manager_app/new_category.html",
        {
            "form": form,
        }
    )


@login_required()
def save_char(request):
    ch = Characteristic()
    if request.POST:
        ch.name = request.POST["name"]
        ch.type_of_char = request.POST["type_of_char"]
        ch.save()
    return HttpResponse(json.dumps(model_to_dict(ch)), content_type="application/json")