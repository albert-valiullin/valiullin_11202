from django.contrib import admin
from rel_app.models import *


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'product', 'count', 'status', 'dateTime', 'address')
    search_fields = ('user__username', 'product__name', 'status', 'dateTime', 'address__city')
    list_filter = ('user__username', 'product__name', 'status', 'address__city')
    date_hierarchy = 'dateTime'


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('order', 'amount', 'transaction_id', 'payment_method', 'dateTime')
    search_fields = ('order__product__name', 'order__user__username', 'amount', 'transaction_id', 'payment_method', 'dateTime')
    list_filter = ('order__user__username', 'order__product__name', 'payment_method')
    date_hierarchy = 'dateTime'


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('country', 'region', 'city', 'addr', 'postcode')
    search_fields = ('country', 'region', 'city', 'addr', 'postcode')
    list_filter = ('country', 'region', 'city')


@admin.register(DeliveryAddress)
class DeliveryAddressAdmin(admin.ModelAdmin):
    list_display = ('profile', 'country', 'region', 'city', 'addr', 'postcode')
    search_fields = ('profile__user__username', 'country', 'region', 'city', 'addr', 'postcode')
    list_filter = ('profile__user__username', 'country', 'region', 'city')
