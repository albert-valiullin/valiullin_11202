# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.CharField(max_length=256)),
                ('region', models.CharField(max_length=256)),
                ('city', models.CharField(max_length=256)),
                ('addr', models.CharField(max_length=256)),
                ('postcode', models.PositiveIntegerField()),
            ],
            options={
                'verbose_name_plural': 'addresses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeliveryAddress',
            fields=[
                ('address_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='rel_app.Address')),
                ('profile', models.ForeignKey(related_name=b'addresses', to='shop_app.Profile')),
            ],
            options={
                'verbose_name_plural': 'delivery addresses',
            },
            bases=('rel_app.address',),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveIntegerField(default=1)),
                ('status', models.CharField(default=b'U', max_length=1, choices=[(b'U', b'Unpaid'), (b'P', b'Paid'), (b'C', b'Cancelled'), (b'D', b'Delivered')])),
                ('dateTime', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('order', models.OneToOneField(primary_key=True, serialize=False, to='rel_app.Order')),
                ('amount', models.PositiveIntegerField()),
                ('transaction_id', models.CharField(max_length=256, null=True, blank=True)),
                ('payment_method', models.CharField(max_length=1, choices=[(b'A', b'Card'), (b'B', b'Cash'), (b'C', b'Coupon')])),
                ('dateTime', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='order',
            name='address',
            field=models.ForeignKey(to='rel_app.DeliveryAddress'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='product',
            field=models.ForeignKey(to='shop_app.Product'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
