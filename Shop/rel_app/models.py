from django.db import models
from django.contrib.auth.models import User

STATUS = (
    ('U', 'Unpaid'),
    ('P', 'Paid'),
    ('C', 'Cancelled'),
    ('D', 'Delivered')
)


class Address(models.Model):
    country = models.CharField(max_length=256)
    region = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    addr = models.CharField(max_length=256)
    postcode = models.PositiveIntegerField()

    def __unicode__(self):
        return u'%s, %s, %s, %s' % (self.country, self.region, self.city, self.addr)

    class Meta:
        verbose_name_plural = "addresses"


class DeliveryAddress(Address):
    profile = models.ForeignKey("shop_app.Profile", related_name="addresses")

    def __unicode__(self):
        return u'%s: %s, %s, %s, %s' % (self.profile, self.country, self.region, self.city, self.addr)

    class Meta:
        verbose_name_plural = "delivery addresses"


class Order(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey("shop_app.Product")
    count = models.PositiveIntegerField(default=1)
    status = models.CharField(max_length=1, choices=STATUS, default='U')
    dateTime = models.DateTimeField(auto_now_add=True)
    address = models.ForeignKey(DeliveryAddress)

    def __unicode__(self):
        return u'%s: %s | %s' % (self.user, self.product, self.dateTime)

PAYMENT_METHOD = (
    ('A', 'Card'),
    ('B', 'Cash'),
    ('C', 'Coupon')
)


class Payment(models.Model):
    order = models.OneToOneField(Order, primary_key=True)
    amount = models.PositiveIntegerField()
    transaction_id = models.CharField(max_length=256, null=True, blank=True)
    payment_method = models.CharField(max_length=1, choices=PAYMENT_METHOD)
    dateTime = models.DateTimeField(auto_now_add=True)
