from django.contrib import admin

from rel_app.models import DeliveryAddress
from shop_app.models import *


class DeliveryAddressInline(admin.TabularInline):
    model = DeliveryAddress


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'full_name', 'gender', 'avatar', 'email', 'birthday')
    search_fields = ('user__username', 'full_name', 'gender', 'email', 'birthday')
    list_filter = ('gender', 'birthday')
    date_hierarchy = 'birthday'
    inlines = (DeliveryAddressInline,)



@admin.register(ProductCharacteristic)
class ProductCharacteristicAdmin(admin.ModelAdmin):
    list_display = ('product', 'characteristic', 'value')
    search_fields = ('product__name', 'characteristic__name')
    list_filter = ('product', 'characteristic')


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent')
    search_fields = ('name', 'parent__name')
    list_filter = ('parent',)


class ProdCharInline(admin.TabularInline):
    model = ProductCharacteristic


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'image', 'category')
    search_fields = ('name', 'category__name')
    list_filter = ('category',)
    inlines = (ProdCharInline,)


@admin.register(Characteristic)
class CharacteristicAdmin(admin.ModelAdmin):
    list_display = ('name', 'type_of_char')
    search_fields = ('name', 'type_of_char')
    list_filter = ('type_of_char',)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'product', 'dateTime')
    search_fields = ('author__username', 'product_name', 'dateTime')
    list_filter = ('author', 'product', 'dateTime')
    date_hierarchy = 'dateTime'


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('author', 'recipient', 'dateTime')
    search_fields = ('author__username', 'recipient__username', 'dateTime')
    list_filter = ('author', 'recipient', 'dateTime')
    date_hierarchy = 'dateTime'

