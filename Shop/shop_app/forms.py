from django import forms
from django.forms import RadioSelect
from shop_app.models import Profile
from shop_app import models


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput, max_length=30)


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput, max_length=30)
    confirm = forms.CharField(widget=forms.PasswordInput, max_length=30)


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['full_name', 'gender', 'avatar', 'email', 'birthday', 'about']
        widgets = {
            'gender': RadioSelect(dict(models.GENDER)),
        }