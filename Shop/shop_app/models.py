from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=256)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children')

    def get_all_children(self):
        r = [self]
        for c in Category.objects.filter(parent=self):
            r.append(c.get_all_children())
        return r

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"


CHARACTERISTIC_TYPE = (
    ('B', 'Boolean'),
    ('P', 'Positive Number'),
    ('C', 'Choose')
)


class Characteristic(models.Model):
    name = models.CharField(max_length=256)
    type_of_char = models.CharField(blank=True, max_length=1, choices=CHARACTERISTIC_TYPE, default='C')

    def __unicode__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=256)
    price = models.PositiveIntegerField()
    description = models.TextField(blank=True)
    image = models.ImageField(null=True, blank=True)
    category = models.ForeignKey(Category)

    def __unicode__(self):
        return self.name


class ProductCharacteristic(models.Model):
    product = models.ForeignKey(Product, related_name='characteristics')
    characteristic = models.ForeignKey(Characteristic)
    value = models.CharField(max_length=256, blank=True)

    def __unicode__(self):
        return u'%s - %s' % (self.product, self.characteristic)


class Comment(models.Model):
    author = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    content = models.TextField(max_length=4096)
    dateTime = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s - %s | %s' % (self.author, self.product, self.dateTime)


class Message(models.Model):
    author = models.ForeignKey(User, related_name='msg_author')
    recipient = models.ForeignKey(User, related_name='msg_recipient')
    content = models.TextField()
    dateTime = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s - %s | %s' % (self.author, self.recipient, self.dateTime)

GENDER = (
    ('M', 'Male'),
    ('F', 'Female')
)


class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    full_name = models.CharField(blank=True, max_length=256)
    gender = models.CharField(blank=True, max_length=1, choices=GENDER)
    avatar = models.ImageField(blank=True, upload_to='avatars')
    email = models.EmailField(blank=True, max_length=254)
    birthday = models.DateField(blank=True, null=True)
    about = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s: %s' % (self.user, self.full_name)