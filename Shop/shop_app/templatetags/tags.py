from django import template

register = template.Library()


@register.inclusion_tag('shop_app/children.html')
def children_tag(category):
    children = category.children.all()
    return {'children': children}


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()
    dict_[field] = value
    return dict_.urlencode()


@register.simple_tag
def url_params(request):
    dict_ = request.GET.copy()
    return dict_.urlencode()