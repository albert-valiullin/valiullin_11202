from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Q, F
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.translation import check_for_language
from django.views.generic import DetailView, ListView
import operator
from Shop import settings
from rel_app.models import Order

from shop_app.forms import LoginForm, ProfileEditForm, SignUpForm
from shop_app.models import Product, Profile, Category


def lang(template_name):
    def decor(view):
        def wrapper(request):
            templ = template_name
            if settings.LANGUAGE_COOKIE_NAME in request.COOKIES and request.COOKIES[settings.LANGUAGE_COOKIE_NAME] == 'ru':
                idx = templ.index('/')
                templ = templ[:idx] + '/ru' + templ[idx:]
            response = view(request, template_name=templ)
            return response
        return wrapper
    return decor


def lang_for_class_view(template_getter, view):
    template_names = template_getter
    if settings.LANGUAGE_COOKIE_NAME in view.request.COOKIES and view.request.COOKIES[settings.LANGUAGE_COOKIE_NAME] == 'ru':
        templ = template_names[0]
        idx = templ.index('/')
        templ = templ[:idx] + '/ru' + templ[idx:]
        template_names[0] = templ
    return template_names


def no_auth_please(v):
    def wrapper(request, template_name, *a, **k):
        user = request.user
        if user.is_authenticated():
            return HttpResponseRedirect(reverse("index"))
        else:
            return v(request, template_name, *a, **k)
    return wrapper


@lang(template_name="shop_app/my_orders.html")
@login_required
def my_orders(request, template_name):
    user_id = request.user.id
    ord_list = Order.objects.raw('SELECT * from rel_app_order WHERE user_id = %s', [user_id])
    return render(
        request,
        template_name,
        {
            "order_list": ord_list
        }
    )


@lang(template_name='shop_app/sign_in.html')
@no_auth_please
def sign_in(request, template_name):
    if request.POST:
        f = LoginForm(request.POST)
        if f.is_valid():
            user = authenticate(
                username=f.cleaned_data["username"],
                password=f.cleaned_data["password"]
            )
            if user:
                login(request, user)
                response = None
                if "next" in request.GET:
                    response = HttpResponseRedirect(request.GET["next"])
                else:
                    response = HttpResponseRedirect(reverse('index'))
                if "language" not in request.COOKIES:
                    response.set_cookie(settings.LANGUAGE_COOKIE_NAME, "", max_age=30*24*60*60)
                return response

            else:
                return HttpResponseRedirect(reverse('sign_in'))
        else:
            return HttpResponseRedirect(reverse('sign_in'))
    else:
        f = LoginForm()
        context = {"f": f}
        if 'next' in request.GET:
            context["next"] = request.GET["next"]
        return render(request, template_name, context)


@lang(template_name='shop_app/sign_up.html')
@no_auth_please
def sign_up(request, template_name):
    f = SignUpForm(request.POST or None)
    if request.POST:
        if f.is_valid():
            us = f.cleaned_data["username"]
            pas = f.cleaned_data["password"]
            conf = f.cleaned_data["confirm"]
            if pas == conf:
                us = User.objects.create_user(username=us, password=pas)
                p = Profile(user=us)
                p.save()
                return HttpResponseRedirect(reverse('edit_profile'))
            return HttpResponseRedirect(reverse('sign_up'))
        else:
            return HttpResponseRedirect(reverse('sign_up'))
    else:
        return render(request, template_name, {"form": f})


@login_required()
def sign_out(request):
    logout(request)
    return HttpResponseRedirect(reverse('sign_in'))


@login_required()
def profile(request):
    p = None
    try:
        p = request.user.profile
    except Profile.DoesNotExist:
        pass
    if not p:
        p = Profile(user=request.user)
        p.save()
    return HttpResponseRedirect(p.id)


class ProfileView(DetailView):
    model = Profile
    template_name = "shop_app/profile.html"

    def get_template_names(self):
        return lang_for_class_view(super(ProfileView, self).get_template_names(), self)


@lang(template_name="shop_app/edit_profile.html")
@login_required()
def edit_profile(request, template_name):
    p = request.user.profile
    if request.method == 'POST':
        if not p:
            p = Profile(user=request.user)
        form = ProfileEditForm(request.POST, request.FILES, instance=p)
        if form.is_valid():
            p.save()
            form.save()
        return HttpResponseRedirect(reverse('profile'))
    else:
        if not p:
            p = Profile(user=request.user)
            p.save()
        form = ProfileEditForm(instance=p)
        return render(
            request,
            template_name,
            {
                "form": form,
                'image': p.avatar
            }
        )


@login_required()
def avatar_download(request, prof):
    user_profile = Profile.objects.get(id=prof)
    filename = user_profile.avatar.name.split('/')[-1]
    response = HttpResponse(user_profile.avatar, content_type='image')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response


class ProductDetailView(DetailView):
    model = Product
    template_name = "shop_app/product.html"

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        chain = []
        category = self.get_object().category
        while category.parent:
            chain.insert(0, category)
            category = category.parent
        chain.insert(0, category)
        context['category_chain'] = chain
        return context

    def get_template_names(self):
        return lang_for_class_view(super(ProductDetailView, self).get_template_names(), self)


class ProductListView(ListView):
    model = Product
    paginate_by = 5
    template_name = 'shop_app/product_list.html'

    def get_template_names(self):
        return lang_for_class_view(super(ProductListView, self).get_template_names(), self)


class SearchProductListView(ListView):
    model = Product
    paginate_by = 5
    template_name = "shop_app/product_list.html"

    def get_queryset(self):
        get_dict = self.request.GET
        filter_list = []
        if 'q' in get_dict:
            filter_list.append(Q(name__contains=get_dict['q']))
        if 'max' in get_dict and 'min' in get_dict:
            filter_list.append(Q(price__gt=get_dict['min']))
            filter_list.append(Q(price__lt=get_dict['max']))
        qs = None
        if 'category' in get_dict:
            cat_view = CategoryProductListView(kwargs={'cat':get_dict['category']})
            qs = cat_view.get_queryset()
        if qs:
            qs = qs.filter(reduce(operator.and_, filter_list, Q(name__contains='')))
        else:
            qs = Product.objects.filter(reduce(operator.and_, filter_list, Q(name__contains='')))
        return qs

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(SearchProductListView, self).get_context_data(**kwargs)
        context['query'] = self.request.GET.get('q', '')
        return context

    def get_template_names(self):
        return lang_for_class_view(super(SearchProductListView, self).get_template_names(), self)


class CategoryListView(ListView):
    model = Category
    template_name = "shop_app/categories.html"
    queryset = Category.objects.filter(parent__isnull=True)

    def get_template_names(self):
        return lang_for_class_view(super(CategoryListView, self).get_template_names(), self)


class CategoryProductListView(ListView):
    model = Product
    paginate_by = 5
    template_name = "shop_app/product_list.html"

    def get_queryset(self):
        parent_category = Category.objects.get(id=self.kwargs['cat'])
        categories = []
        self.__category_list(parent=parent_category, cat_list=categories)
        qs = Product.objects.filter(category__in=categories).filter(name=F('name'))
        return qs

    def __category_list(self, parent, cat_list):
        cat_list.append(parent)
        if parent.children.count > 0:
            for c in parent.children.all():
                self.__category_list(c, cat_list)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CategoryProductListView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.get(id=self.kwargs['cat'])
        return context

    def get_template_names(self):
        return lang_for_class_view(super(CategoryProductListView, self).get_template_names(), self)


@lang(template_name="shop_app/cart.html")
@login_required()
def cart(request, template_name):
    if "cart" not in request.session:
        request.session["cart"] = {}
    sh_cart = request.session["cart"]
    items = [
        {
            "product": Product.objects.get(id=item),
            "count": sh_cart[item],
            "sum": Product.objects.get(id=item).price*int(sh_cart[item])
        } for item in sh_cart.keys()]
    cart_sum = sum(item['sum'] for item in items)
    return render(
        request,
        template_name,
        {
            'items': items,
            'sum': cart_sum
        }
    )


@login_required()
def add_to_cart(request):
    product = Product.objects.get(id=request.POST["product"])
    count = int(request.POST["count"])
    if "cart" not in request.session:
        request.session["cart"] = {}
    sh_cart = request.session["cart"]
    if str(product.id) in sh_cart.keys():
        sh_cart[str(product.id)] = int(sh_cart[str(product.id)]) + count
    else:
        sh_cart[product.id] = count
    request.session["cart"] = sh_cart
    return HttpResponseRedirect(reverse('product', args=[product.id]))


@login_required()
def del_from_cart(request):
    product = Product.objects.get(id=request.POST["product"])
    if "cart" not in request.session:
        request.session["cart"] = {}
    sh_cart = request.session["cart"]
    if str(product.id) in sh_cart.keys():
        del sh_cart[str(product.id)]
    request.session["cart"] = sh_cart
    return HttpResponseRedirect(reverse('cart'))


@login_required()
def set_language(request):
    next = request.REQUEST.get('next', None)
    if not next:
        next = request.META.get('HTTP_REFERER', None)
    if not next:
        next = '/'
    response = HttpResponseRedirect(next)
    if request.method == 'GET':
        lang_code = request.GET.get('language', None)
        if lang_code and check_for_language(lang_code):
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code, max_age=30*24*60*60)
    return response
